<?php

//require_once plugin_dir_path( __FILE__ ) . 'vendor/autoload.php';
require_once( ABSPATH . "wp-includes/wp-db.php" );
require_once plugin_dir_path( __FILE__ ) . 'class-logger.php';

class Example_Background_Processing {

	use WP_Example_Logger;
	/**
	 * @var WP_Example_Request
	 */
	public $process_single;

	/**
	 * @var WP_Example_Process
	 */
	public $process_all;

	/**
	 * Example_Background_Processing constructor.
	 */
	public function __construct() {
		add_action( 'plugins_loaded', array( $this, 'init' ) );
		add_action( 'admin_bar_menu', array( $this, 'admin_bar' ), 100 );
		add_action( 'init', array( $this, 'process_handler' ) );

		
	}

	/**
	 * Init
	 */
	public function init() {
		
		
		require_once plugin_dir_path( __FILE__ ) . 'class-logger.php';
		require_once plugin_dir_path( __FILE__ ) . 'async-requests/class-example-request.php';
		require_once plugin_dir_path( __FILE__ ) . 'background-processes/class-example-process.php';

		$this->process_single = new WP_Example_Request();
		$this->process_all    = new WP_Example_Process();
	}

	/**
	 * Admin bar
	 *
	 * @param WP_Admin_Bar $wp_admin_bar
	 */
	public function admin_bar( $wp_admin_bar ) {
		if ( ! current_user_can( 'manage_options' ) ) {
			return;
		}	
		
	}

	/**
	 * Process handler
	 */
	public function process_handler() {

		if(isset($_GET['process'])){
	
			if ( 'all' === $_GET['process'] ) {
				
				$this->handle_all($pro_cat,$pro_brand);			
			}
			if ( 'allcsv' === $_GET['process'] ) {
				
				$this->handle_all_csv($pro_cat,$pro_brand);			
			}
		}
		
	}

	/**
	 * Handle single
	 */
	public function handle_single() {
		$names = $this->get_names();
		$rand  = array_rand( $names, 1 );
		$name  = $names[ $rand ];

		$this->process_single->data( array( 'name' => $name ) )->dispatch();
	}

	/**
	 * Handle all
	 */

	public function handle_all($pro_cat,$pro_brand) {
		
		

		global $wpdb;

		$table_posts = $wpdb->prefix.'posts';
		$table_meta = $wpdb->prefix.'postmeta';	

		$sql_orphan = "DELETE $table_meta FROM $table_meta
		LEFT JOIN $table_posts ON $table_posts.ID = $table_meta.post_id
		WHERE $table_posts.ID IS NULL";

		$orphandata = $wpdb->get_results($sql_orphan,ARRAY_A);		

		sleep(5);


		if(isset($_GET['product_brand'])){
			if($_GET['main_category'] != ''){
			
				$pro_cat = $_GET['main_category'];

			}
		}

		if(isset($_GET['product_brand'])){
			if($_GET['product_brand']){

				$pro_brand = $_GET['product_brand'];
			}
	 	}

		write_log('handleall');
		
		global $wpdb;			
			
		$file_name = $pro_cat.'_'.$pro_brand.'.json';	
		$upload_dir = wp_upload_dir(); 
		$array = $fields = array(); $i = 0;
		$upload_dir['basedir'].'/'.'sfn-data/'.$file_name;
		write_log($upload_dir['basedir'].'/'.'sfn-data/'.$file_name);
		
		$strJsonFileContents = file_get_contents($upload_dir['basedir'].'/'.'sfn-data/'.$file_name);
		
		$handle_data = str_replace('&quot;', '"', $strJsonFileContents);

		$handle = json_decode(($handle_data), true);
		switch (json_last_error()) {
			case JSON_ERROR_NONE:
			  echo "No errors";
			  break;
			case JSON_ERROR_DEPTH:
			  echo "Maximum stack depth exceeded";
			  break;
			case JSON_ERROR_STATE_MISMATCH:
			  echo "Invalid or malformed JSON";
			  break;
			case JSON_ERROR_CTRL_CHAR:
			  echo "Control character error";
			  break;
			case JSON_ERROR_SYNTAX:
			  echo "Syntax error";
			  break;
			case JSON_ERROR_UTF8:
			  echo "Malformed UTF-8 characters";
			  break;
			default:
			  echo "Unknown error";
			  break;
		  }		
		
		$_session['sfn_post_type'] = $pro_cat;		
		
			if ($handle) {
				$k = 1;				
				foreach( $handle  as $row) {

					write_log('current loop ->' .$k);
									
					$valuesData = $this->insert_product($row, $pro_cat);	
					
					if($k % 100==0){ sleep(3);}
					
					$k++;
				}	
		
		}

		

			global $wpdb;
			$product_sync_table = $wpdb->prefix."sfn_sync";
			$status_sync = 'success - '.$k;
			$wpdb->query( "DELETE  FROM {$product_sync_table} WHERE product_brand = '{$pro_brand}' and  product_category = '{$pro_cat}'" );
			$wpdb->insert( $product_sync_table, array('product_category' => $pro_cat, 'product_brand' => $pro_brand , 'sync_status' => $status_sync),array( '%s', '%s','%s'));
			write_log('Sync Completed - '.$pro_brand);
			wp_mail( 'velocity.syncproduct@gmail.com' ,  get_bloginfo(). '-'.$pro_cat.'-'.$pro_brand.' Product Sync Ended '.date("Y-m-d h:i:s",time()) , $pro_cat.'-'.$pro_brand.' Product Sync Ended' );

			
		
	}

	/**
	 * Handle all CSV Data
	 */
	 protected function handle_all_csv() {

			

		global $wpdb;

		$table_posts = $wpdb->prefix.'posts';
		$table_meta = $wpdb->prefix.'postmeta';	

		$sql_orphan = "DELETE $table_meta FROM $table_meta
		LEFT JOIN $table_posts ON $table_posts.ID = $table_meta.post_id
		WHERE $table_posts.ID IS NULL";

		$orphandata = $wpdb->get_results($sql_orphan,ARRAY_A);		

		sleep(5);


		if(isset($_GET['product_brand'])){
			if($_GET['main_category'] != ''){
			
				$pro_cat = $_GET['main_category'];

			}
		}

		if(isset($_GET['product_brand'])){
			if($_GET['product_brand']){

				$pro_brand = $_GET['product_brand'];
			}
	 	}

		write_log('handleall');
		
		global $wpdb;			
			
		$file_name = 'dolphin-carpet-noswatch.json';	
		$upload_dir = wp_upload_dir(); 
		$array = $fields = array(); $i = 0;
		$upload_dir['basedir'].'/'.'sfn-data/'.$file_name;
		write_log($upload_dir['basedir'].'/'.'sfn-data/'.$file_name);
		
		$strJsonFileContents = file_get_contents($upload_dir['basedir'].'/'.'sfn-data/'.$file_name);
		
		$handle_data = str_replace('&quot;', '"', $strJsonFileContents);

		$handle = json_decode(($handle_data), true);
		switch (json_last_error()) {
			case JSON_ERROR_NONE:
			  echo "No errors";
			  break;
			case JSON_ERROR_DEPTH:
			  echo "Maximum stack depth exceeded";
			  break;
			case JSON_ERROR_STATE_MISMATCH:
			  echo "Invalid or malformed JSON";
			  break;
			case JSON_ERROR_CTRL_CHAR:
			  echo "Control character error";
			  break;
			case JSON_ERROR_SYNTAX:
			  echo "Syntax error";
			  break;
			case JSON_ERROR_UTF8:
			  echo "Malformed UTF-8 characters";
			  break;
			default:
			  echo "Unknown error";
			  break;
		  }		
		
		$_session['sfn_post_type'] = $pro_cat;		
		
			if ($handle) {
				$k = 1;				
				foreach( $handle  as $row) {

					write_log('current loop ->' .$k);
									
					$valuesData = $this->insert_product_csv($row, $pro_cat);	
					
					if($k % 100==0){ sleep(3);}
					
					$k++;
				}	
		
		}

		

			global $wpdb;
			$product_sync_table = $wpdb->prefix."sfn_sync";
			$status_sync = 'success - '.$k;
			$wpdb->query( "DELETE  FROM {$product_sync_table} WHERE product_brand = '{$pro_brand}' and  product_category = '{$pro_cat}'" );
			$wpdb->insert( $product_sync_table, array('product_category' => $pro_cat, 'product_brand' => $pro_brand , 'sync_status' => $status_sync),array( '%s', '%s','%s'));
			write_log('Sync Completed - '.$pro_brand);
			wp_mail( 'velocity.syncproduct@gmail.com' ,  get_bloginfo(). '-'.$pro_cat.'-'.$pro_brand.' Product Sync Ended '.date("Y-m-d h:i:s",time()) , $pro_cat.'-'.$pro_brand.' Product Sync Ended' );

				
		
	}
	/**
	 * Get names
	 *
	 * @return array
	 */
	public function get_names() {
		return array(
			'Grant Buel',
			'Bryon Pennywell',
			'Jarred Mccuiston',
			'Reynaldo Azcona',
			'Jarrett Pelc',
			'Blake Terrill',
			'Romeo Tiernan',
			'Marion Buckle',
			'Theodore Barley',
			'Carmine Hopple',
			'Suzi Rodrique',
			'Fran Velez',
			'Sherly Bolten',
			'Rossana Ohalloran',
			'Sonya Water',
			'Marget Bejarano',
			'Leslee Mans',
			'Fernanda Eldred',
			'Terina Calvo',
			'Dawn Partridge',
		);
	}

}

