<?php

// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );

remove_action('wp_head', 'wp_generator');
// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );

add_action( 'wp_enqueue_scripts', function(){
    wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
    wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.min.js","","",1);
    wp_enqueue_script("PDF_script","https://cdnjs.cloudflare.com/ajax/libs/html2pdf.js/0.9.2/html2pdf.bundle.min.js","","",1);
    wp_enqueue_script("shareBox-script", get_stylesheet_directory_uri()."/resources/sharebox/needsharebutton.js","","",1);
    wp_enqueue_script("Child-js",get_stylesheet_directory_uri()."/script.js","","",1);
});

//Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
    if ( isset( $atts['facet'] ) ) {       
        $output= '<div class="facet-wrap"><strong>'.@$atts['title'].'</strong>'. $output .'</div>';
    }
    return $output;
}, 10, 2 );

// Move Yoast to bottom
function yoasttobottom()
{
    return 'low';
}

add_filter('wpseo_metabox_prio', 'yoasttobottom');

function replace_core_jquery_version() {
    wp_deregister_script( 'jquery' );
    // Change the URL if you want to load a local copy of jQuery from your own server.
    wp_register_script( 'jquery', "https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js", array(), '3.2.1' );
}
add_action( 'wp_enqueue_scripts', 'replace_core_jquery_version' );

function getUserIpAddr() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
	
	if ( strstr($ipaddress, ',') ) {
            $tmp = explode(',', $ipaddress,2);
            $ipaddress = trim($tmp[1]);
    }
    return $ipaddress;
}


// Custom function for lat and long

function get_storelisting() {
    
    global $wpdb;
    $content="";


  //  echo 'no cookie';
    
    $urllog = 'https://global-ds.cloud.netacuity.com/webservice/query';
    //12.68.65.195
    $response = wp_remote_post( $urllog, array(
        'method' => 'GET',
        'timeout' => 45,
        'redirection' => 5,
        'httpversion' => '1.0',
        'headers' => array('Content-Type' => 'application/json'),         
        'body' =>  array('u'=> 'b25d7667-74cc-4fcc-9adf-7b5f4f8f5bd0','ip'=> getUserIpAddr(),'dbs'=> 'all','trans_id'=> 'example','json'=> 'true' ),
        'blocking' => true,               
        'cookies' => array()
        )
    );
        
        $rawdata = json_decode($response['body'], true);
        $userdata = $rawdata['response'];

        $autolat = $userdata['pulseplus-latitude'];
        $autolng = $userdata['pulseplus-longitude'];
        if(isset($_COOKIE['dolphin_store'])){

            $store_location = $_COOKIE['dolphin_store'];
        }else{

            $store_location = '';
           
          }

        //print_r($rawdata);
        
        $sql =  "SELECT post_lat.meta_value AS lat,post_lng.meta_value AS lng,posts.ID, 
                ( 3959 * acos( cos( radians(".$autolat." ) ) * cos( radians( post_lat.meta_value ) ) * 
                cos( radians( post_lng.meta_value ) - radians( ".$autolng." ) ) + sin( radians( ".$autolat." ) ) * 
                sin( radians( post_lat.meta_value ) ) ) ) AS distance FROM wp_posts AS posts
                INNER JOIN wp_postmeta AS post_lat ON post_lat.post_id = posts.ID AND post_lat.meta_key = 'latitude'
                INNER JOIN wp_postmeta AS post_lng ON post_lng.post_id = posts.ID AND post_lng.meta_key = 'longitue'  
                WHERE posts.post_type = 'store-locations' 
                AND posts.post_status = 'publish' GROUP BY posts.ID HAVING distance < 50000000000000000000 ORDER BY distance";

        $storeposts = $wpdb->get_results($sql);
        $storeposts_array = json_decode(json_encode($storeposts), true);      

       
        if($store_location ==''){
            //write_log('empty loc');           
            $store_location = $storeposts_array['0']['ID'];
            $store_distance = round($storeposts_array['0']['distance'],1);
            
        }else{

           // write_log('noempty loc');
            $key = array_search($store_location, array_column($storeposts_array, 'ID'));
            $store_distance = round($storeposts_array[$key]['distance'],1);           

        }
        
        $content .= '<div class="header_store"><div class="store_wrapper">';
        $content .='<div class="locationWrapFlyer">
        <div class="icons">
             <i class="fa fa-map-marker" aria-hidden="true"></i>
        </div>
		<div class="contentFlyers">
        <div class="contentFlyer">
            <p>You\'re Shopping</p>';
        //$content .= '<span class="title-prefix">'.get_the_title($store_location).'</span>';
    
        $content .= '<div class="title-prefix"><a href="javascript:void(0)" target="_self" id="openFlyer" aria-label="open flyer" class="store-cta-link headeropenFlyer"><span class="location-title">'.get_the_title($store_location).'</span><i class="fa fa-chevron-down" aria-hidden="true" style="font-style: italic;font-size:12px;"></i></a></div>';
        $content .= '<div class="mobile"><a href="tel:'.get_field('phone', $store_location).'">'.get_field('phone', $store_location).'</a></div></div>';
	$content .= '<div class="view-all"><a href="/locations/">View All Locations</a></div>';
		$content .= '</div>';
        echo $content;
        wp_die();
}

//add_shortcode('storelisting', 'get_storelisting');
add_action( 'wp_ajax_nopriv_get_storelisting', 'get_storelisting' );
add_action( 'wp_ajax_get_storelisting', 'get_storelisting' );



function choose_location_listing(){

    global $wpdb;
    
    $urllog = 'https://global-ds.cloud.netacuity.com/webservice/query';
    //12.68.65.195
    $response = wp_remote_post( $urllog, array(
        'method' => 'GET',
        'timeout' => 45,
        'redirection' => 5,
        'httpversion' => '1.0',
        'headers' => array('Content-Type' => 'application/json'),         
        'body' =>  array('u'=> 'b25d7667-74cc-4fcc-9adf-7b5f4f8f5bd0','ip'=> getUserIpAddr(),'dbs'=> 'all','trans_id'=> 'example','json'=> 'true' ),
        'blocking' => true,               
        'cookies' => array()
        )
    );

        $rawdata = json_decode($response['body'], true);
        $userdata = $rawdata['response'];
        $autolat = $userdata['pulseplus-latitude'];
        $autolng = $userdata['pulseplus-longitude'];
        
        $sql =  "SELECT post_lat.meta_value AS lat,post_lng.meta_value AS lng,posts.ID, 
                ( 3959 * acos( cos( radians(".$autolat." ) ) * cos( radians( post_lat.meta_value ) ) * 
                cos( radians( post_lng.meta_value ) - radians( ".$autolng." ) ) + sin( radians( ".$autolat." ) ) * 
                sin( radians( post_lat.meta_value ) ) ) ) AS distance FROM wp_posts AS posts
                INNER JOIN wp_postmeta AS post_lat ON post_lat.post_id = posts.ID AND post_lat.meta_key = 'latitude'
                INNER JOIN wp_postmeta AS post_lng ON post_lng.post_id = posts.ID AND post_lng.meta_key = 'longitue'  
                WHERE posts.post_type = 'store-locations' 
                AND posts.post_status = 'publish' GROUP BY posts.ID HAVING distance < 50000000000000000 ORDER BY distance LIMIT 0, 25";

        $storeposts = $wpdb->get_results($sql);

        $storeposts_array = json_decode(json_encode($storeposts), true);  

    $content = '<div id="storeLocation" class="changeLocation">';
    $content .= '<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>';
    $content .= '<div class="content">'; 
  
    foreach ( $storeposts as $post ) {
     	$store_url = str_replace("/store-locations/", "/", get_the_permalink($post->ID) );
        $content .= '<div class="store_wrapper" id ="'.$post->ID.'">';
        $content .= '<h5 class="title-prefix"><a href="'.get_field('store_page_link', $post->ID).'">'.get_the_title($post->ID).'  - <b>'.round($post->distance,1).' MI</b></a></h5>';
        $content .= '<h5 class="store-add">'.get_field('address', $post->ID).' '.get_field('city',$post->ID).', '.get_field('state',$post->ID).' '.get_field('postal_code', $post->ID).'</h5>';
        
        if(get_field('closed', $post->ID)){
            $content .= '<p class="store-hour redtext">'.get_field('hours', $post->ID).'</p>';
            $content .= '<p>Still happly serving this area in the meanwhile at our Humble location. Please call for details</p>';
        }else{
            $rows = get_field('store_hours',$post->ID);
            if( $rows ) {
                foreach( $rows as $row ) {
                    $day = $row['day'];
                  // write_log(date("l"));
                   // write_log($day); write_log($row);
                    if($day == date("l")){

                        if($row['is_closed'] == 1){

                        $content .= '<p class="store-hour">CLOSED TODAY</p>';   

                        }else{

                        $content .= '<p class="store-hour">OPEN UNTIL '.$row['close_time'].'</p>';

                        }

                    }

                }
            }
            
        }
        $content .= '<p class="store-hour">Send us a text: &nbsp;'.get_field('store_phone', $post->ID).'</p>';
        $content .= '<p class="store-hour"><i class="fa fa-phone-alt"></i> &nbsp;'.get_field('phone', $post->ID).'</p>';
        $content .= '<p class="store-hour email-css"><i class="fa icon-envelope"></i> '.get_field('store_email_address', $post->ID).'</p>';
        $content .= '<a href="javascript:void(0)" data-id="'.$post->ID.'" data-distance="'.round($post->distance,1).'" data-storename="'.get_the_title($post->ID).'" data-phone="'.get_field('phone', $post->ID).'" target="_self" class="store-cta-link choose_location">Choose This Location</a>';
        if(get_field('store_page_link',$post->ID)){
            $content .= '<br><a href="'.get_field('store_page_link',$post->ID).'" target="_self" class="store-cta-link view_location"> View Location</a>';
        }
        $content .= '</div>'; 
    }
    
    $content .= '</div>';
    $content .= '</div>';

    echo $content;
    wp_die();
}

//add_shortcode('chooselisting', 'choose_location_listing');
add_action( 'wp_ajax_nopriv_choose_location_listing', 'choose_location_listing' );
add_action( 'wp_ajax_choose_location_listing', 'choose_location_listing' );

//choose this location FUnctionality

add_action( 'wp_ajax_nopriv_choose_location', 'choose_location' );
add_action( 'wp_ajax_choose_location', 'choose_location' );

function choose_location() {
   
        $content = '<div class="store_wrapper">';
        $content .='<div class="locationWrapFlyer">
        <div class="icons">
        <i class="fa fa-map-marker" aria-hidden="true"></i>
        </div>
        <div class="contentFlyer">
            <p>You\'re Shopping</p>';
          
        
        $content .= '<h3 class="title-prefix">'. get_the_title($_POST['store_id']) . '</h3>';
        // $content .= '<a href="javascript:void(0)" target="_self" id="openFlyer" class="store-cta-link headeropenFlyer"><h5 class="title-prefix">'.get_the_title($_POST['store_id']).'  - <b>'.$_POST['distance'].' MI</b><i class="fa fa-caret-down" aria-hidden="true"></i></h5></a>';
        // $content .= '<h5 class="store-add">'.get_field('address', $_POST['store_id']).' '.get_field('city',$_POST['store_id']).', '.get_field('state',$_POST['store_id']).' '.get_field('postal_code', $_POST['store_id']).'</h5>';
        

        if(get_field('closed', $_POST['store_id'])){
            // $content .= '<p class="store-hour redtext">'.get_field('hours',$_POST['store_id']).'</p>';
            // $content .= '<p>Still happly serving this area in the meanwhile at our Humble location. Please call for details</p>';
            // $content .= '<p class="store-hour"> '.get_field('phone', $_POST['store_id']).'</p>';
        }else{
           // $content .= '<p class="store-hour">'.get_field('hours',$_POST['store_id']).' | '.get_field('phone', $_POST['store_id']).'</p>';
        }

       // $content .= '<a href="javascript:void(0)" target="_self" id="openFlyer" class="store-cta-link">Change Location</a>';
       $content .= '<h5 class="title-prefix" style="font-style: italic;font-size:12px;"><a href="javascript:void(0)" target="_self" id="openFlyer" class="store-cta-link headeropenFlyer"><i class="fa fa-chevron-down" aria-hidden="true"></i></a></h5>';
       $content .= '<div class="mobile"><a href="tel:'.$_POST['phone'].'">'.$_POST['phone'].'</a></div>'; 
       $content .= '</div>';

        echo $content;

    wp_die();
}

/*:::::::::::::::::::::::Workbook code start heree:::::::::::::::::::::::::::: */

add_action( 'wp_ajax_nopriv_add_fav_product', 'add_fav_product' );
add_action( 'wp_ajax_add_fav_product', 'add_fav_product' );

function add_fav_product() {
    $is_fav = $_POST['is_fav'];
    $post_id = $_POST['post_id'];

    $result = update_post_meta( $post_id, 'is_fav', $is_fav);
if($result == false){
    $result1 = add_post_meta( $post_id, 'is_fav', $is_fav);
}
}

add_action( 'wp_ajax_nopriv_base64_to_jpeg_convert', 'base64_to_jpeg_convert' );
add_action( 'wp_ajax_base64_to_jpeg_convert', 'base64_to_jpeg_convert' );

function base64_to_jpeg_convert() {    
    global $wpdb;

    $upload_dir = wp_get_upload_dir();
    $output_file= $upload_dir['basedir']. '/measure/'.uniqid().'.png';

    $img = $_POST['imagedata']; 
    $img = str_replace('data:image/png;base64,', '', $img);
    $img = str_replace(' ', '+', $img);
    $data = base64_decode($img);
    file_put_contents($output_file, $data);
    
    if ( is_user_logged_in() ) {
        $current_user = wp_get_current_user();
        write_log( 'Personal Message For '. $current_user->ID ) ;
     
        $data=array(
            'userid' => $current_user->ID, 
            'image_name' => $_POST['imagename'],
            'image_path' => $output_file 
        );    
    
         $wpdb->insert( 'wp_measure_images', $data);

         $measureimages = $wpdb->get_results("SELECT * FROM wp_measure_images WHERE userid = $current_user->ID ");

        write_log($measureimages);

        
        $content = '<div id="mesureMentprintMe" class="product-plp-grid product-grid swatch "><div class="row product-row" id="aniimated-thumbnials">';

        $content .= '<h3 style="text-align: center; font-size: 33px;"><span>SAVED MEASUREMENTS</span></h3>';

        foreach($measureimages as $img){

           

            $img_path = str_replace('/var/www/html', '', $img->image_path);

            $image_url = home_url().''.$img_path;

            $content .= '<div class="col-lg-4 col-md-4 col-sm-6 "><div class="fl-post-grid-post">';
            $content .= '<div class="fl-post-grid-image prod_like_wrap mesureMentWrap">';

            $content .= '<a href="javascript:void(0);" title="#" class="wplightbox"><img class="measure_img" src="'.home_url().''.$img_path.'" /></a>';

            $content .= '<div class="favButtons button-wrapper">
                <a class="button fl-button view" href="javascript:void(0);" title="#"  data-img="'.home_url().''.$img_path.'" onclick="openPopUp(this)" data-title="'.$img->image_name.'">VIEW</a>';
                $content .= '<a class="button fl-button deletemeasure" href="javascript:void(0)" data-id="'.$img->id.'"><i class="fa fa-trash" aria-hidden="true"></i></a>'; 
                $content .= '</div> </div>';
                $content .= '<h3><span>'.$img->image_name.'</span></h3>';

                $content .= "</div></div>";

        }

        $content .= "</div></div>";

        echo $content;


        wp_die();
    }

}

function measurement_tool_images($arg){    
    global $wpdb;

    if ( is_user_logged_in() ) {
        $current_user = wp_get_current_user();
        write_log( 'Personal Message For '. $current_user->ID ) ;

        $measureimages = $wpdb->get_results("SELECT * FROM wp_measure_images WHERE userid = $current_user->ID ");

        write_log($measureimages);

        if($measureimages){

        
        $content = '<div id="mesureMentprintMe" class="product-plp-grid product-grid swatch "><div class="row product-row" id="aniimated-thumbnials">';
        $content .= '<h3 style="text-align: center; font-size: 33px;"><span>SAVED MEASUREMENTS</span></h3>';

        foreach($measureimages as $img){

           

            $img_path = str_replace('/var/www/html', '', $img->image_path);

            $image_url = home_url().''.$img_path;

            $content .= '<div class="col-lg-4 col-md-4 col-sm-6 "><div class="fl-post-grid-post">';
            $content .= '<div class="fl-post-grid-image prod_like_wrap mesureMentWrap">';

            $content .= '<a href="javascript:void(0);" title="#" class="wplightbox"><img class="measure_img" src="'.home_url().''.$img_path.'" /></a>';

            $content .= '<div class="favButtons button-wrapper">
                <a class="button fl-button view" href="javascript:void(0);" title="#"  data-img="'.home_url().''.$img_path.'" onclick="openPopUp(this)" data-title="'.$img->image_name.'">VIEW</a>';
                $content .= '<a class="button fl-button deletemeasure" href="#" data-id="'.$img->id.'"><i class="fa fa-trash" aria-hidden="true"></i></a>'; 
                $content .= '</div> </div>';
                $content .= '<h3><span>'.$img->image_name.'</span></h3>';

                $content .= "</div></div>";

        }

        $content .= "</div></div>";
        

        return $content;
      }
    }
   
  
}    
add_shortcode('measurementtool_images', 'measurement_tool_images');


add_action( 'wp_ajax_nopriv_delete_measureimg', 'delete_measurement_images' );
add_action( 'wp_ajax_delete_measureimg', 'delete_measurement_images' );

function delete_measurement_images() {  
    global $wpdb;

    $mid = $_POST['mimg_id'];

    if ( is_user_logged_in() ) {
        $current_user = wp_get_current_user();
        write_log( 'Personal Message For '. $current_user->ID ) ;

        $wpdb->get_results('DELETE FROM wp_measure_images WHERE userid = '.$current_user->ID.' and id = '.$mid.'');
        

        $measureimages = $wpdb->get_results("SELECT * FROM wp_measure_images WHERE userid = $current_user->ID ");

        write_log($measureimages);

        
        $content = '<div id="mesureMentprintMe" class="product-plp-grid product-grid swatch "><div class="row product-row" id="aniimated-thumbnials">';

        $content .= '<h3 style="text-align: center; font-size: 33px;"><span>SAVED MEASUREMENTS</span></h3>';

        foreach($measureimages as $img){

           

            $img_path = str_replace('/var/www/html', '', $img->image_path);

            $content .= '<div class="col-lg-4 col-md-4 col-sm-6 "><div class="fl-post-grid-post">';
            $content .= '<div class="fl-post-grid-image prod_like_wrap mesureMentWrap">';

            $content .= '<a href="'.home_url().''.$img_path.'" title="#" class="wplightbox"><img class="measure_img" src="'.home_url().''.$img_path.'" /></a>';

            $content .= '<div class="favButtons button-wrapper">
                <a class="button fl-button" href="javascript:void(0)" title="#" onclick="openPopUp(this)"   data-img="'.home_url().''.$img_path.'" data-title="'.$img->image_name.'" onclik="openPopUp(e)">VIEW</a>';
                $content .= '<a class="button fl-button deletemeasure" href="#" data-id="'.$img->id.'"><i class="fa fa-trash" aria-hidden="true"></i></a>'; 
                $content .= '</div> </div>';
                $content .= '<h3><span>'.$img->image_name.'</span></h3>';

                $content .= "</div></div>";

        }

        $content .= "</div></div>";

        echo $content;
    }

  wp_die();

}

add_action( 'wp_ajax_nopriv_add_favroiute', 'add_favroiute' );
add_action( 'wp_ajax_add_favroiute', 'add_favroiute' );

function add_favroiute() {

    global $wpdb;

    if ( is_user_logged_in() ) {
        $current_user = wp_get_current_user();
        write_log( 'Personal Message For '. $current_user->ID ) ;
     
        $data=array(
            'user_id' => $current_user->ID, 
            'product_id' => $_POST['post_id']           
        );    
    
         $wpdb->insert( 'wp_favorite_posts', $data);        

         return true;

         wp_die();

    }else{

        return false;

        wp_die();
    }
}

add_action( 'wp_ajax_nopriv_remove_favroiute', 'remove_favroiute' );
add_action( 'wp_ajax_remove_favroiute', 'remove_favroiute' );

function remove_favroiute() {    

    global $wpdb;

    if ( is_user_logged_in() ) {
        $current_user = wp_get_current_user();
        write_log( 'Personal Message For '. $current_user->ID ) ;
     
        $data=array(
            'user_id' => $_POST['user_id'], 
            'product_id' => $_POST['post_id']           
        );    
    
        $wpdb->get_results('DELETE FROM wp_favorite_posts WHERE user_id = '.$_POST['user_id'].' and product_id = '.$_POST['post_id'].'');

         return true;

         wp_die();

    }else{

        return false;

        wp_die();
    }

}

// Favorite product shortcodes 
function greatcustom_favorite_posts_function(){

    global $wpdb;
    $content = "";
    $fav_sql = 'SELECT product_id FROM wp_favorite_posts WHERE user_id = '.get_current_user_id().'';
            
    $check_fav = $wpdb->get_results($fav_sql);    

    write_log($check_fav);

    $fav_products = array();

    foreach($check_fav as $fav){

        $fav_products[] = $fav->product_id;
    }

    write_log('dsfsd'.$fav_products);
    
    $favorite_post_ids = array_reverse($fav_products);
    $page = intval(get_query_var('paged'));

    $post_array = array('carpeting','hardwood_catalog','laminate_catalog','luxury_vinyl_tile','tile_catalog',
     'instock_laminate', 'instock_carpet', 'instock_hardwood', 'instock_luxury_vinyl', 'instock_tile');

        $brandmapping = array(
            "Carpet"=>"carpeting",
            "Hardwood"=>"hardwood_catalog",
            "Laminate"=>"laminate_catalog",
            "Luxury Vinyl Tile"=>"luxury_vinyl_tile",
            "Ceramic Tile"=>"tile_catalog",
            "Waterproof"=>"solid_wpc_waterproof",
            "Instock Carpet"=>"instock_carpet",
            "Instock Hardwood"=>"instock_hardwood",
            "Instock Laminate"=>"instock_laminate",
            "Instock Luxury Vinyl"=>"instock_luxury_vinyl",
            "Instock Tile"=>"instock_tile"
        );
    

   

    $content .= '<div id="ajaxreplace"><div class="favProWarpper printWrap">
                    <div class="shareAndPrint hideShare">
                        <a href="#"  id="share-button-3" class="need-share-button-default" data-share-position="middleBottom" data-share-networks="Mailto,Twitter,Pinterest,Facebook,GooglePlus,Linkedin"></a>
                        <a href="#" class="printDOc" ><i class="fa fa-print" aria-hidden="true"></i></a>
                    </div>
                <div class="row statiContent">
                    <div class="col-lg-12">
                        <h2>My Favorite Products </h2>
                        <p>Our product catalog is home to thousands of products. 
                        Collect your favorites and save them here to help you select the right product for your home.</p>
                    </div>
                </div>';
    
    if(!empty($check_fav)) {

        
        foreach($post_array as $posttype){

            $qry = array('post__in' => $favorite_post_ids, 'posts_per_page'=> $post_per_page, 'orderby' => 'post__in', 'paged' => $page);
            // custom post type support can easily be added with a line of code like below.
           //  $qry['post_type'] = array('carpeting','hardwood_catalog','laminate_catalog','luxury_vinyl_tile','tile_catalog');
           $qry['post_type'] = array($posttype);
           
           query_posts($qry);
    
           $post_type_name = array_search($posttype,$brandmapping);
            
           $content .= '<div id="printMe" class="product-plp-grid product-grid swatch "><div class="row product-row">';
            if ( have_posts() ) :
                $content .= '<h3>'.$post_type_name.'</h3>';
            endif;
            while ( have_posts() ) : the_post();

            $note_sql = 'SELECT note FROM wp_favorite_posts WHERE user_id = '.get_current_user_id().' and product_id='.get_the_ID().'';
            
            $check_note = $wpdb->get_results($note_sql, ARRAY_A);  
            // write_log('check_note');
            // write_log($check_note[0]['note']);
               
            $content .= '<div class="col-lg-4 col-md-4 col-sm-6 "><div class="fl-post-grid-post">';
            $content .= '<div class="fl-post-grid-image prod_like_wrap">';
    
                $image = swatch_image_product_thumbnail(get_the_ID(),'222','222');
    
                $sku = get_post_meta(get_the_ID(),'sku',true);
                $manufacturer = get_post_meta(get_the_ID(),'manufacturer',true);
    
                $content .= '<a href="'.get_permalink().'" title="'.get_the_title() .'"><img crossorigin="Anonymous"  class="list-pro-image" src="'. $image.'" alt="'.get_the_title().'" /></a>';
    
                $content .= '<div class="favButtons button-wrapper">
                    <a class="button fl-button" href="'.get_permalink().'" title="'.get_the_title() .'">VIEW PRODUCT</a>';
                   // $content .=   roomvo_script_integration($manufacturer,$sku,get_the_ID());
                   if($check_note[0]['note'] == '' || $check_note[0]['note'] == null){
                    $content .= '<a class="button fl-button add_note_fav" id="add_note_fun" href="javascript:void(0)" data-productid="'.get_the_ID().'">ADD A NOTE</a>'; 
                   }
                    $content .= '<div class="buttonHolder"><a id="rem_fav" data-id="'.get_the_ID().'" class="remove-parent" data-user_id="'.get_current_user_id().'" href="javascript:void(0)"><i class="fa fa-trash" aria-hidden="true"></i></a>';

                    if($check_note[0]['note'] != '' || $check_note[0]['note'] != null){
                        $content .= '<a class="view_note_fav" data-note="'.$check_note[0]['note'].'" id="view_note_fun" href="javascript:void(0)" data-productid="'.get_the_ID().'"><i class="fa fa-sticky-note" aria-hidden="true"></i></a>'; 
                        // $content .= '<div id="'.get_the_ID().'" style="display:none">'.$check_note[0]['note'].'</div>';
                       }
                    $content .= '</div></div> </div>';
                    $content .= ' <div class="fl-post-grid-text product-grid btn-grey">';
                    $content .= '<h4><span>'.get_the_title().'</span></h4>';
                    $content .= '</div>';
    
                //wpfp_remove_favorite_link(get_the_ID());
                $content .= "</div></div>";
            endwhile;
            $content .= "</div></div>";
        
    
        }
        echo '</div></div>';

    return $content;

 }


}
add_shortcode('greatcustom_favorite_posts', 'greatcustom_favorite_posts_function');


add_action( 'wp_ajax_nopriv_remove_favroiute_list', 'remove_greatcustom_favorite_posts_function' );
add_action( 'wp_ajax_remove_favroiute_list', 'remove_greatcustom_favorite_posts_function' );
function remove_greatcustom_favorite_posts_function(){

    global $wpdb;

    $content = "";

    $wpdb->get_results('DELETE FROM wp_favorite_posts WHERE product_id = '.$_POST['post_id'].' and user_id = '.get_current_user_id().'');    

    $fav_sql = 'SELECT product_id FROM wp_favorite_posts WHERE user_id = '.get_current_user_id().'';
            
    $check_fav = $wpdb->get_results($fav_sql);    

    write_log($check_fav);

    $fav_products = array();

    foreach($check_fav as $fav){

        $fav_products[] = $fav->product_id;
    }

    write_log('dsfsd'.$fav_products);
    
    $favorite_post_ids = array_reverse($fav_products);
    $page = intval(get_query_var('paged'));

    $post_array = array('carpeting','hardwood_catalog','laminate_catalog','luxury_vinyl_tile','tile_catalog');

        $brandmapping = array(
            "Carpet"=>"carpeting",
            "Hardwood"=>"hardwood_catalog",
            "Laminate"=>"laminate_catalog",
            "Luxury Vinyl Tile"=>"luxury_vinyl_tile",
            "Ceramic Tile"=>"tile_catalog",
            "Waterproof"=>"solid_wpc_waterproof"
        );
    

   

    $content .= '<div class="favProWarpper printWrap">
                    <div class="shareAndPrint hideShare">
                        <a href="#"  id="share-button-3" class="need-share-button-default" data-share-position="middleBottom" data-share-networks="Mailto,Twitter,Pinterest,Facebook,GooglePlus,Linkedin"></a>
                        <a href="#" class="printDOc" ><i class="fa fa-print" aria-hidden="true"></i></a>
                    </div>
                <div class="row statiContent">
                    <div class="col-lg-12">
                        <h2>My Favorite Products</h2>
                        <p>Our product catalog is home to thousands of products. 
                        Collect your favorites and save them here to help you select the right product for your home.</p>
                    </div>
                </div>';
    
    if(!empty($check_fav)) {

        
        foreach($post_array as $posttype){

            $qry = array('post__in' => $favorite_post_ids, 'posts_per_page'=> $post_per_page, 'orderby' => 'post__in', 'paged' => $page);
            // custom post type support can easily be added with a line of code like below.
           //  $qry['post_type'] = array('carpeting','hardwood_catalog','laminate_catalog','luxury_vinyl_tile','tile_catalog');
           $qry['post_type'] = array($posttype);
           
           query_posts($qry);
    
           $post_type_name = array_search($posttype,$brandmapping);
            
           $content .= '<div id="printMe" class="product-plp-grid product-grid swatch "><div class="row product-row">';
            if ( have_posts() ) :
                $content .= '<h3>'.$post_type_name.'</h3>';
            endif;
            while ( have_posts() ) : the_post();

            $note_sql = 'SELECT note FROM wp_favorite_posts WHERE user_id = '.get_current_user_id().' and product_id='.get_the_ID().'';
            
            $check_note = $wpdb->get_results($note_sql, ARRAY_A);  
            write_log('check_note');
            write_log($check_note[0]['note']);
               
            $content .= '<div class="col-lg-4 col-md-4 col-sm-6 "><div class="fl-post-grid-post">';
            $content .= '<div class="fl-post-grid-image prod_like_wrap">';
    
                $image = swatch_image_product_thumbnail(get_the_ID(),'222','222');
    
                $sku = get_post_meta(get_the_ID(),'sku',true);
                $manufacturer = get_post_meta(get_the_ID(),'manufacturer',true);
    
                $content .= '<a href="'.get_permalink().'" title="'.get_the_title() .'"><img class="list-pro-image" crossorigin="Anonymous"  src="'. $image.'" alt="'.get_the_title().'" /></a>';
    
                $content .= '<div class="favButtons button-wrapper">
                    <a class="button fl-button" href="'.get_permalink().'" title="'.get_the_title() .'">VIEW PRODUCT</a>';
                   // $content .=   roomvo_script_integration($manufacturer,$sku,get_the_ID());
                   if($check_note[0]['note'] == '' || $check_note[0]['note'] == null){
                    $content .= '<a class="button fl-button add_note_fav" id="add_note_fun" href="javascript:void(0)" data-productid="'.get_the_ID().'">ADD A NOTE</a>'; 
                   }
                    $content .= '<div class="buttonHolder"><a id="rem_fav" data-id="'.get_the_ID().'" class="remove-parent" data-user_id="'.get_current_user_id().'" href="javascript:void(0)"><i class="fa fa-trash" aria-hidden="true"></i></a>';

                    if($check_note[0]['note'] != '' || $check_note[0]['note'] != null){
                        $content .= '<a class="view_note_fav" data-note="'.$check_note[0]['note'].'" id="view_note_fun" href="javascript:void(0)" data-productid="'.get_the_ID().'"><i class="fa fa-sticky-note" aria-hidden="true"></i></a>'; 
                        $content .= '<div id="'.get_the_ID().'" style="display:none">'.$check_note[0]['note'].'</div>';
                       }
                    $content .= '</div></div> </div>';
                    $content .= ' <div class="fl-post-grid-text product-grid btn-grey">';
                    $content .= '<h4><span>'.get_the_title().'</span></h4>';
                    $content .= '</div>';
    
                //wpfp_remove_favorite_link(get_the_ID());
                $content .= "</div></div>";
            endwhile;
            $content .= "</div></div>";
        
    
        }

        $content .='</div></div>';
        

    echo $content;

    wp_die();

 }


}

add_action('wp_ajax_add_note_form', 'add_note_form');
add_action('wp_ajax_nopriv_add_note_form', 'add_note_form');

function add_note_form()
{
    global $wpdb;

    $fav_sql = 'UPDATE wp_favorite_posts SET note = "'.$_POST['note'].'" WHERE user_id = '.get_current_user_id().' and product_id = '.$_POST['addnote_productid'].'';
            
    $check_fav = $wpdb->get_results($fav_sql); 

    echo $fav_sql;
}

/*::::::::::::::::::::::::::::Workbook code end heree:::::::::::::::::::::::::::: */



/*:::::::::::::::::::::::::::check plp template :::::::::::::::::::::::::::: */

// add_filter( 'facetwp_template_html', function( $output, $class ) {
//     //$GLOBALS['wp_query'] = $class->query;
//     $prod_list = $class->query;  
//     ob_start();       
//    // write_log($class->query_args['check_instock']);
//     if(isset($class->query_args['check_instock']) && $class->query_args['check_instock'] == 1){

//         $dir = get_stylesheet_directory().'/product-instock-loop-dolphine.php';
//         //$dir = get_stylesheet_directory().'/product-loop-dolphin.php';

//     }else{

//         $dir = get_stylesheet_directory().'/product-loop-dolphin.php';
//     }
   
//     require_once $dir;    
//     return ob_get_clean();
// }, 10, 2 );

// Render Color in in-stock product detail page form
add_filter( 'gform_pre_render_38', 'populate_product_color' );
add_filter( 'gform_pre_validation_38', 'populate_product_color' );
add_filter( 'gform_pre_submission_filter_38', 'populate_product_color' );
add_filter( 'gform_admin_pre_render_38', 'populate_product_color' );

add_filter( 'gform_pre_render_35', 'populate_product_color' );
add_filter( 'gform_pre_validation_35', 'populate_product_color' );
add_filter( 'gform_pre_submission_filter_35', 'populate_product_color' );
add_filter( 'gform_admin_pre_render_35', 'populate_product_color' );

add_filter( 'gform_pre_render_37', 'populate_product_color' );
add_filter( 'gform_pre_validation_37', 'populate_product_color' );
add_filter( 'gform_pre_submission_filter_37', 'populate_product_color' );
add_filter( 'gform_admin_pre_render_37', 'populate_product_color' );

function populate_product_color( $form ) {
    foreach ( $form['fields'] as &$field ) {
         // Only populate field ID 12
        if( $field['id'] == 26 ) {
            $flooringtype = get_post_type(get_the_ID()); 
            $collection =  get_post_meta(get_the_ID(),'collection',true); 
            $satur = array('Masland','Dixie Home');  
                                        
            if($collection != NULL ){
                if($collection == 'COREtec Colorwall' || $collection == 'Coretec Colorwall') {
                    $familycolor = get_post_meta(get_the_ID(),'style',true); 
                    $key = 'style';
                }else{
                    $familycolor = $collection;   
                    $key = 'collection';
                }	
            }else{	
                if (in_array(get_post_meta(get_the_ID(),'brand',true), $satur)){
                    $familycolor = get_post_meta(get_the_ID(),'design',true); 
                    $key = 'design';
                }
            }	
            $args = array(
                'post_type'      => $flooringtype,
                'posts_per_page' => -1,
                'post_status'    => 'publish',
                'meta_query'     => array(
                    array(
                        'key'     => $key,
                        'value'   => $familycolor,
                        'compare' => '='
                    ),
                    array(
                        'key' => 'swatch_image_link',
                        'value' => '',
                        'compare' => '!='
                    )
                )
            );										
        
            $the_query = new WP_Query( $args );
                                        
                                        
            $choices = array(); // Set up blank array

            
                // loop over each select item at add value/option to $choices array

            while ($the_query->have_posts()) {
                $the_query->the_post();

                $image = swatch_image_product_thumbnail(get_the_ID(),'222','222');

                $brand = get_post_meta(get_the_ID(),'brand',true);
                $collection = get_post_meta(get_the_ID(),'collection',true);
                $color = get_post_meta(get_the_ID(),'color',true);
				$skuInstock = get_post_meta(get_the_ID(),'sku',true);
                $text = $brand.' '.$collection.' '.$color;
                
                $choices[] = array( 'text' => $text, 'price' =>  $image, 'value' => get_the_title(get_the_ID()) );

            }
            wp_reset_postdata();

            // Set placeholder text for dropdown
            $field->placeholder = '-- Choose color --';

            // Set choices from array of ACF values
            $field->choices = $choices;
        }
    }
    return $form;
}

add_filter( 'gform_pre_render_28', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_28', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_28', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_28', 'populate_product_location_form' );

add_filter( 'gform_pre_render_29', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_29', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_29', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_29', 'populate_product_location_form' );

add_filter( 'gform_pre_render_30', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_30', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_30', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_30', 'populate_product_location_form' );

add_filter( 'gform_pre_render_31', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_31', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_31', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_31', 'populate_product_location_form' );

add_filter( 'gform_pre_render_32', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_32', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_32', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_32', 'populate_product_location_form' );

add_filter( 'gform_pre_render_33', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_33', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_33', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_33', 'populate_product_location_form' );

add_filter( 'gform_pre_render_34', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_34', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_34', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_34', 'populate_product_location_form' );


function populate_product_location_form( $form ) {

    foreach ( $form['fields'] as &$field ) {

        // Only populate field ID 12
        if ( $field->type != 'select' || strpos( $field->cssClass, 'populate-store' ) === false ) {
            continue;
        }      	

            $args = array(
                'post_type'      => 'store-locations',
                'posts_per_page' => -1,
                'post_status'    => 'publish'
            );										
          
             $locations =  get_posts( $args );

             $choices = array(); 

             foreach($locations as $location) {
                

                  $title = get_the_title($location->ID);
                  
                       $choices[] = array( 'text' => $title, 'value' => $title );

              }
              wp_reset_postdata();

             // write_log($choices);

             // Set placeholder text for dropdown
             $field->placeholder = '-- Choose Location --';

             // Set choices from array of ACF values
             $field->choices = $choices;
        
   }
return $form;

}

add_filter("wpseo_breadcrumb_links", "override_yoast_breadcrumb_pdp_page");

function override_yoast_breadcrumb_pdp_page($links){
    $post_type = get_post_type();

    if ($post_type === 'instock_luxury_vinyl') {
        $breadcrumb[] = array(
            'url' => '/in-stock/luxury-vinyl-flooring/',
            'text' => 'In-Stock Luxury Vinyl Flooring'
        );
        array_splice($links, 1, -3, $breadcrumb);
    }
    else if ($post_type === 'instock_hardwood') {
        $breadcrumb[] = array(
            'url' => '/in-stock/hardwood-flooring/',
            'text' => 'In-Stock Hardwood Flooring'
        );
        array_splice($links, 1, -3, $breadcrumb);
    }
    else if ($post_type === 'instock_laminate') {
        $breadcrumb[] = array(
            'url' => '/in-stock/laminate-flooring/',
            'text' => 'In-Stock Laminate Flooring'
        );
        array_splice($links, 1, -3, $breadcrumb);
    }
    else if ($post_type === 'instock_tile') {
        $breadcrumb[] = array(
            'url' => '/in-stock/tile/',
            'text' => 'In-Stock Tile Flooring'
        );
        array_splice($links, 1, -3, $breadcrumb);
    }
    else if ($post_type === 'instock_carpet') {
        $breadcrumb[] = array(
            'url' => '/in-stock/carpet/',
            'text' => 'In-Stock Carpet'
        );
        array_splice($links, 1, -3, $breadcrumb);
    }
    else if (is_singular( 'tile_catalog' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/tile/',
            'text' => 'Tile',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/tile/shop',
            'text' => 'Shop',
        );
        array_splice( $links, 1, -1, $breadcrumb );
        
    }
    else if (is_singular( 'carpeting' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/carpet/',
            'text' => 'Carpet',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/carpet/shop',
            'text' => 'Shop',
        );
        array_splice( $links, 1, -1, $breadcrumb );
        
    }
    else if (is_singular( 'hardwood_catalog' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/hardwood-flooring/',
            'text' => 'Hardwood',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/hardwood-flooring/shop',
            'text' => 'Shop',
        );
        array_splice( $links, 1, -1, $breadcrumb );
        
    }
    else if (is_singular( 'laminate_catalog' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/laminate-flooring/',
            'text' => 'Laminate',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/laminate-flooring/shop',
            'text' => 'Shop',
        );
        array_splice( $links, 1, -1, $breadcrumb );
        
    }
    else if (is_singular( 'luxury_vinyl_tile' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/luxury-vinyl-flooring/',
            'text' => 'Luxury Vinyl',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/luxury-vinyl-flooring/shop',
            'text' => 'Shop',
        );
        array_splice( $links, 1, -1, $breadcrumb );
        
    }
    
    //return $links;
    return $links;

    
}