		<?php do_action('fl_content_close'); ?>
	
	</div><!-- .fl-page-content -->
	<?php 
		
	do_action('fl_after_content'); 
	
	if ( FLTheme::has_footer() ) :
	
	?>
	<footer class="fl-page-footer-wrap" itemscope="itemscope" itemtype="http://schema.org/WPFooter">
		<?php 
			
		do_action('fl_footer_wrap_open');
		do_action('fl_before_footer_widgets');

		
		FLTheme::footer_widgets();
		
		do_action('fl_after_footer_widgets');
		do_action('fl_before_footer');
		
		FLTheme::footer();
		
		do_action('fl_after_footer');
		do_action('fl_footer_wrap_close');
		
		?>

	</footer>
	<?php endif; ?>
	<?php do_action('fl_page_close'); ?>
</div><!-- .fl-page -->
<?php 
	
wp_footer(); 

do_action('fl_body_close');

FLTheme::footer_code();

?>

<script src="https://calculator.measuresquare.com/scripts/jquery-m2FlooringCalculator.js"></script> 

<script>
$(function () {
    $('.calculateBtn').m2Calculator({
        measureSystem: "Imperial",            
        thirdPartyName: "Dolphin", 
        thirdPartyEmail: "cary@dolphincarpet.com",  // if showDiagram = false, will send estimate data to this email when user click Email Estimate button
        showCutSheet: false, // if false, will not include cutsheet section in return image
        showDiagram: true,  // if false, will close the popup directly 
        
        cancel: function () {
            //when user closes the popup without calculation.
        },
        callback: function (data) {                  
            var imageName = '';        
            if (!data.input.rooms.length){
            imageName = data.input.stairs[0].name;
            }else{
            imageName = data.input.rooms[0].name;
            }
            $.ajax({
                type: "POST",
                url: "/wp-admin/admin-ajax.php",
                data: 'action=base64_to_jpeg_convert&imagedata=' + data.img + '&imagename='+ imageName,
                success: function(data) {
                    jQuery("#mesureMentprintMe").html(data);
                }

            });
        }
    });


    $('.calculateBtninstock').m2Calculator({
        measureSystem: "Imperial",
        thirdPartyName: "Dolphin Carpet",
        thirdPartyEmail: "devteam.agency@gmail.com", // if showDiagram = false, will send estimate data to this email when user click Email Estimate button
        showCutSheet: false, // if false, will not include cutsheet section in return image
        showDiagram: true, // if false, will close the popup directly 

        cancel: function() {
            //when user closes the popup without calculation.
        },
        callback: function(data) {
            //json format, include user input, usage and base64image
            $("#callback").html(JSON.stringify(data));
            console.log(data.input)
            //$("#input_32_3").val(data.usage);
            $("#input_35_3").val(data.usage);
            //$("#input_34_3").val(data.usage);
            //$("#input_36_3").val(data.usage);
            //$("#input_37_3").val(data.usage);

            // $("#image").attr("src", data.img);  //base64Image 
            //window.location.href = "https://dolphincarpet-stg.mm-dev.agency/thank-you/";
        }
    });
});
      
</script>
</body>
</html>